/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
import $ from 'jquery';

/**
 * Javascript code for up/down comment vote
 */
// select html element of class js-vote-arrows
// find any of the a tags inside, and register a click listener on them.
// On click, we make an AJAX request to /comments/10; hardcoding
var $container = $('.js-vote-arrows');
$container.find('a').on('click', function(e) {
    e.preventDefault();
    var $link = $(e.currentTarget);
    $.ajax({
        url: '/comments/10/vote/' + $link.data('direction'),
        method: 'POST'
    }).then(function(data) {
        // $container.find('.js-vote-total').text(response.votes);
        $container.find('.js-vote-total').text(data.votes);
    });
});