# Symfony 5 Application
A web app on questions and answers in Animal Health

The app uses Webpack Encore, Bootstrap, Fontawesome, jQuery, ...
No direct linking to a CDN or downloading jQuery. They are installed into our node_modules/ directory and later used by requiring them (see assets/app.js and assets/styles/app.css).

## Controller
- ChanceController
- QuestionController
- CommentController
- Routing is done in the Controller using Annotation

## Features
- Symfony "Serializer" for converting objects into JSON. 
- Use of JSON API endpoint
- Use of JavaScript to make an AJAX call to endpoint. 
- Use of global Javascript as well as page-specific JavaScript.
- Use of route and controller system.
- Logger object ("logger" service)
- Use of Twig recipe
- Use of asset() Twig function to help generate URLs 
- Use of the path() Twig function to generate URLs based on the routing configuration.
- Autowiring & use of the Twig Service
- Profiler for debugging

## Symfony Server
- Run the server and have it display the log messages in the console; you won’t be able to run other commands at the same time: 
  * symfony server:start
- Run the Symfony server in the background and continue working and running other commands:
  * symfony server:start -d

## Webpack Encore
- Symfony library that combines and minifies your CSS and JavaScript files...
- css (Bootstrap, Fontawesone, ...) and js (jQuery) files are managed in the assets folder and not in Symfony's public folder.

## To build the assets:
- compile assets once
  * yarn encore dev
- recompile assets automatically when files change
  * yarn encore dev --watch

## Test with valid URL:
- localhost:8000/question
- localhost:8000/questions/{slug}
  * example: http://localhost:8000/questions/ensuring-food-safety

## References
- [symfonycasts.com](https://symfonycasts.com/screencast/symfony)
- [researchgate.net](https://www.researchgate.net/topic/Animal-Health)
- [fontawesome.com](https://fontawesome.com/how-to-use/on-the-web/setup/using-package-managers)
- [codegrepper.com](https://www.codegrepper.com/code-examples/whatever/font+awesome+symfony+encore)