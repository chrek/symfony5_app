<?php

namespace App\DataFixtures\ORM;

use App\Entity\Author;
use App\Entity\BlogPost;
use Doctrine\Bundle\FixturesBundle\Fixture;

use Doctrine\Persistence\ObjectManager;

class Fixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $author = new Author();
        $author
            ->setName('Jem Das')
            ->setTitle('Chef de Projet')
            ->setUsername('auth0-username')
            ->setCompany('Fight Covid-19 Company')
            ->setBio('Il n\'y a personne qui naime la souffrance pour elle-même, qui ne la recherche et qui ne la veuille pour elle-même...Il ny a personne qui naime la souffrance pour elle-même, qui ne la recherche et qui ne la veuille pour elle-même...')
            ->setPhone('33 777777777')
            ->setFacebook('jemdas')
            ->setTwitter('jem.das')
            ->setGithub('jem-das');
            
        $manager->persist($author);

        $blogPost = new BlogPost();
        $blogPost
            ->setTitle('Code with Jems')
            ->setSlug('first-post')
            ->setDescription('Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.')
            ->setBody('On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même.')
            ->setAuthor($author);
        $manager->persist($blogPost);
        $manager->flush();
    }
}
