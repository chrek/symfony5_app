<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

// class ChanceController
class ChanceController extends AbstractController
{
    // Route defined in Controller
    // /**
    //  * @Route("/chance/number")
    //  */
    // public function number()
    // {
    //     $number = random_int(0, 100);
    //     // Render HTML using Twig template
    //     return $this->render('chance/number.html.twig', [
    //         'number' => $number,
    //     ]);
    // }

    /**
     * @Route("/chance/number/{max}", name="app_chance_number")
     */
    public function number($max)
    {
        $number = random_int(0, $max);
        // Render HTML using Twig template
        return new Response(
            '<html><body><p>Chance number is : ' .$number. '</p></body></html>'
        );        
    }
}
