<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class QuestionController extends AbstractController
{
    // public function questionpage()
    // {
    //     return new Response(
    //         '<html><body><p>This is Question page</p></body></html>'
    //     );        
    // }

    /**
     * @Route("/question", name="app_question_homepage")
     */
    public function homepage(Environment $twigEnvironment)
    {
        // $html = $twigEnvironment->render('question/questionpage.html.twig');
        // return new Response($html);
        return $this->render('question/questionhomepage.html.twig');
        
    }

    // /**
    //  * @Route("/questions/how-to-tie-my-shoes-with-magic")
    //  */
    // public function show()
    // {
    //     return new Response(
    //         '<html><body><p>This is from show() controller</p></body></html>'
    //     ); 
    // }

    // /**
    //  * @Route("/questions/{slug}")
    //  */
    // public function show($slug)
    // { 
    //     $answers = [
    //         'Make sure your cat is sitting  perfectly  still 🤣',
    //         'Honestly, I like furry shoes better than  my cat',
    //         'Maybe... try saying the spell backwards?',
    //     ];
    //     // return new Response(sprintf(
    //     //     'This is from show() controller with {slug}; saying: "%s"!', $slug
    //     // )); 
    //     return $this->render('question/show.html.twig', [
    //         'question' => ucwords(str_replace('-', ' ', $slug)), 
    //         'answers' => $answers,
    //     ]); 

    //     dump($slug, $this);
    // }

    /**
     * @Route("/questions/{slug}", name="app_question_show")
     */
    public function show($slug)
    { 
        $answers = [
            'Coticosteroid ointments for ophthalmic use may be applied.
            Gökhan Pekel, Pamukkale University',
            'A hamster can develop an eye infection fairly easy. It can be from dirty bedding, which can be avoided by cleaning the hamster’s cage one per week. https://firsthamster.com/',
            'It can also happen because of a stray bacteria on the hamster’s food, for example on a piece of apple or broccoli. Or it could be from many other reasons. The point is that your hamster has an infection and needs your help. https://firsthamster.com/',
        ];
        dump($this);
        // dump($slug, $this);
        // dd($slug, $this);
        return $this->render('question/show.html.twig', [
            'question' => ucwords(str_replace('-', ' ', $slug)), 
            'answers' => $answers,
        ]);        
    }
}
